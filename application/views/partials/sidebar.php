


</head>
<body class="nav-md">
      <div class="container body">

        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentellela Alela!</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/img.jpg" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><?php echo $auth['nama']?></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <h3>MENU</h3>
                            <ul class="nav side-menu">
                                <li><a href="<?php echo base_url()?>"><i class="fa fa-home"></i> Beranda </a>
                                </li>
                                <li><a><i class="fa fa-bookmark-o"></i> Pemesanan <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="<?php echo base_url('pemesanan')?>">Kelola Pemesanan</a>
                                        </li>
                                        <li><a href="<?php echo base_url('pemesanan/tambah')?>">Tambah Pesanan</a>
                                        </li>
                                        <li><a href="<?php echo base_url('pemesanan/cetak')?>">Cetak Bukti Pesan</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-money"></i> Transaksi Pembayaran <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="<?php echo base_url('transaksi')?>"> Kelola Transaksi </a>
                                        </li>
                                        <li><a href="<?php echo base_url('transaksi/tambah')?>"> Tambah Transaksi</a>
                                        </li>
                                        <li><a href="<?php echo base_url('transaksi/cetak')?>"> Cetak Bukti Bayar</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-home"></i> Wisma <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="<?php echo base_url('wisma')?>"> Kelola Wisma </a>
                                        </li>
                                        <li><a href="<?php echo base_url('wisma/tambah')?>"> Tambah Wisma </a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-users"></i> Pegawai <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="<?php echo base_url('pegawai/lihat')?>"> Kelola Pegawai </a>
                                        </li>
                                        <li><a href="<?php echo base_url('pegawai/tambah')?>"> Tambah Pegawai </a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-line-chart"></i> Rekapitulasi & Grafik <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="<?php echo base_url('transaksi/rekapitulasi')?>"> Rekap Transaksi </a>
                                        </li>
                                        <li><a href="<?php echo base_url('pembayaran/rekapitulasi')?>"> Rekap Pembayaran </a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-comments-o"></i> Kritik & Saran <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="<?php echo base_url('kritiksaran')?>"> Lihat Kritik Saran </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="menu_section">
                            <h3>Live On</h3>
                            <ul class="nav side-menu">
                              <li><a><i class="fa fa-bug"></i> Error Logins </a>
                              </li>
                                <li><a href='<?php echo base_url('examples/calendar')?>'><i class="fa fa-calendar"></i> Lihat Check In</a>
                                </li>
                                <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="page_404.html">404 Error</a>
                                        </li>
                                        <li><a href="page_500.html">500 Error</a>
                                        </li>
                                        <li><a href="plain_page.html">Plain Page</a>
                                        </li>
                                        <li><a href="login.html">Login Page</a>
                                        </li>
                                        <li><a href="pricing_tables.html">Pricing Tables</a>
                                        </li>

                                    </ul>
                                </li>
                                <li><a><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>
