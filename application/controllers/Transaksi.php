<?php
//
// if(!isset($_SESSION))\

Class Transaksi extends CI_Controller {

	public function index() {
		$this->load->library('session');
		$data['auth']   = $this->session->logged_in;
		$data['header'] = 'Transaksi';
		$data['title']  = 'Kelola Transaksi';
		$this->load->view('includes/header')
			->view('includes/form-css')
			->view('partials/sidebar', $data)
			->view('partials/top-navigation', $data)
			->view('transaksi/index', $data)
			->view('includes/scripts')
			->view('includes/footer')
		;
	}

	public function tambah() {
		$this->load->library('session');
		$data['auth'] = $this->session->logged_in;
		$this->load->view('includes/header')
			->view('includes/form-css')
			->view('partials/sidebar', $data)
			->view('partials/top-navigation', $data)
			->view('transaksi/tambah', $data)
			->view('includes/scripts')
			->view('includes/footer')
		;

	}

}

?>
