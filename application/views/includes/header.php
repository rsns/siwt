<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SIWT</title>

    <!-- Bootstrap core CSS -->

    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/animate.min.css')?>" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url()?>assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="<?php echo base_url()?>assets/css/icheck/flat/green.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/floatexamples.css" rel="stylesheet">

    <!--  FOR TABLES -->

    <link href="<?php echo base_url()?>assets/css/icheck/flat/green.css" rel="stylesheet">

    <script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>
