<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$entity = ['event', 'question', 'member'];
$route['default_controller'] = 'authentication';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['login'] = '';
