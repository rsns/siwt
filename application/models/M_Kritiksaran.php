<?php

	class M_Kritiksaran extends CI_Model
	{

		public function getList()
		{
			$this->db->select('*');
			$this->db->from('kritiksaran');


			$query = $this->db->get();
			if($query->num_rows()){
				return $query->result();
			}
			else return false;
		}

		public function getDetail($id)
		{
			$this->db->select('*');
			$this->db->from('kritiksaran');
			$this->db->where('id_wisma', $id);
			$this->db->limit(1);


			$query = $this->db->get();
			if($query->num_rows()==1){
				return $query->result();
			}
			else return false;
		}

		public function update($id, $update)
		{

		}

		public function delete($id)
		{

		}


	}

 ?>
