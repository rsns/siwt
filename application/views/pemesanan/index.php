<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3> <?php echo $header ?> <small>Listing design</small></h3>
			</div>
			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">Go!</button>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="x_panel">
					<div class="x_title">
						<h2><?php echo $title ?></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
						</li>
					</ul>
				</li>
				<li><a class="close-link"><i class="fa fa-close"></i></a>
			</li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content">
		<p><?php echo $description ?></p>
		<!-- start project list -->
		<table class="table table-striped projects">
			<thead>
				<tr>
					<th style="width: 1%">#</th>
					<th style="width: 5%">ID Pesan</th>
					<th style="width: 20%">Tamu Penyewa</th>
					<th> Waktu Pesan</th>
					<th> Status Pesan</th>
					<th style="width: 30%"> Menu </th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($pesan as $p) {?>
				<tr>
					<td>#</td>
					<td>
						<?php echo $p->id_pesan ?>
					</td>
					<td>
						<a><?php echo $p->nama_tamu ?></a>
					</td>
					<td>
						<?php echo $p->waktu_pesan ?>
					</td>
					<td>
					</td>
					<td>
						<a href="<?php echo base_url('pemesanan/detail/' . $p->id_pesan) ?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
						<a href="<?php echo base_url('pemesanan/update/' . $p->id_pesan) ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
					</td>
				</tr>
				<?php }
?>
			</tbody>
		</table>
		<!-- end project list -->
	</div>
</div>
</div>
</div>
</div>
<!-- footer content -->
<footer>
<div class="">
<p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
<span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
</p>
</div>
<div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
<!-- /page content -->
</div>
</div>
<div id="custom_notifications" class="custom-notifications dsp_none">
<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
</ul>
<div class="clearfix"></div>
<div id="notif-group" class="tabbed_notifications"></div>
</div>