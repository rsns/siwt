<?php
//
// if(!isset($_SESSION))\

// require_once 'AdminController.php';
Class Wisma extends CI_Controller{


	public function index(){
			$this->load->model('M_Wisma');
		  $this->load->library('session');
		  $data['auth'] = $this->session->logged_in;
			$data['header'] = 'Wisma';
			$data['title'] = 'Kelola Wisma';
			$data['description'] = 'Tabel ini berisi daftar wisma yang anda kelola.';

			$data['daftarwisma'] = $this->M_Wisma->getList();

      $this->load->view('includes/header')
                   ->view('includes/form-css')
                   ->view('partials/sidebar',$data)
                   ->view('partials/top-navigation',$data)
                   ->view('wisma/index',$data)
                   ->view('includes/scripts')
                   ->view('includes/footer')
                    ;
	}

	public function tambah()
	{
		  $this->load->library('session');
		  $data['auth'] = $this->session->logged_in;
			$data['header'] = 'Wisma';
			$data['title'] = 'Tambah Wisma';
			$data['description'] = 'Masukkanlah data wisma baru sesuai isian form berikut';

      $this->load->view('includes/header')
                   ->view('includes/form-css')
                   ->view('partials/sidebar',$data)
                   ->view('partials/top-navigation',$data)
                   ->view('wisma/tambah',$data)
                   ->view('includes/scripts')
                   ->view('includes/footer');

	}

	public function detail($id)
	{
			$this->load->model('M_Wisma');
		  $this->load->library('session');
		  $data['auth'] = $this->session->logged_in;
			$data['wisma'] = $this->M_Wisma->getDetail($id)[0];
			$data['header'] = 'Wisma';
			$data['title'] = 'Detail Wisma';
			$data['description'] = 'Masukkanlah data wisma baru sesuai isian form berikut';

			if($data['wisma']){
	      $this->load->view('includes/header')
	                   ->view('includes/form-css')
	                   ->view('partials/sidebar',$data)
	                   ->view('partials/top-navigation',$data)
	                   ->view('wisma/detail',$data)
	                   ->view('includes/scripts')
	                   ->view('includes/footer');
			}

	}


}


 ?>
