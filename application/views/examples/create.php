<!-- page content -->
          <div class="right_col" role="main">
              <div class="">

                  <div class="page-title">
                      <div class="title_left">
                          <h3>Form Elements</h3>
                      </div>
                      <div class="title_right">
                          <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                              <div class="input-group">
                                  <input type="text" class="form-control" placeholder="Search for...">
                                  <span class="input-group-btn">
                                      <button class="btn btn-default" type="button">Go!</button>
                                  </span>
                              </div>
                          </div>
                      </div>
                  </div>
                   <div class="row">
                       <div class="col-md-12 col-sm-12 col-xs-12">
                           <div class="x_panel">
                               <div class="x_title">
                                   <h2>Form Design <small>different form elements</small></h2>
                                   <ul class="nav navbar-right panel_toolbox">
                                       <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                       </li>
                                       <li><a class="close-link"><i class="fa fa-close"></i></a>
                                       </li>
                                   </ul>
                                   <div class="clearfix"></div>
                               </div>
                               <div class="x_content">
                                   <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                                       <div class="form-group">
                                           <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Event<span class="required">*</span>
                                           </label>
                                           <div class="col-md-6 col-sm-6 col-xs-12">
                                               <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                           </div>
                                       </div>
                                       <!-- enabled, start,end, info, assignedby -->
                                       <div class="form-group">
                                           <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Informasi
                                           </label>
                                           <div class="col-md-6 col-sm-6 col-xs-12">
                                             <textarea id="message" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"></textarea>
                                            </div>
                                       </div>

                                       <div class="form-group">
                                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Pelaksanaan
                                           </label>
                                           <div class="col-md-6 col-sm-6 col-xs-12">
                                               <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" type="text">
                                           </div>
                                       </div>

                                       <div class="form-group">
                                           <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Middle Name / Initial</label>
                                           <div class="col-md-2 col-sm-2 col-xs-4">
                                               <input id="middle-name" class="form-control col-md-7 col-xs-12" type="time" name="middle-name">
                                           </div>
                                           <div class="col-md-2 col-sm-2 col-xs-4">
                                               <input id="middle-name" class="form-control col-md-7 col-xs-12" type="time" name="middle-name">
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                                           <div class="col-md-6 col-sm-6 col-xs-12">
                                               <div id="gender" class="btn-group" data-toggle="buttons">
                                                   <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                       <input type="radio" name="gender" value="male"> &nbsp; Aktif &nbsp;
                                                   </label>
                                                   <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                       <input type="radio" name="gender" value="female" checked=""> &nbsp; Tidak Aktif &nbsp;
                                                   </label>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Dibuat oleh :
                                           </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" disabled="true" class="form-control has-feedback-left" id="inputSuccess2" placeholder="First Name">
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                           </div>
                                       <div class="ln_solid"></div>
                                       <div class="form-group">
                                           <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                               <button type="submit" class="btn btn-primary">Cancel</button>
                                               <button type="submit" class="btn btn-success">Submit</button>
                                           </div>
                                       </div>
                                   </div>

                                   </form>
                               </div>
                           </div>
                       </div>
                   </div>

                   <script type="text/javascript">
                       $(document).ready(function () {
                           $('#birthday').daterangepicker({
                               singleDatePicker: true,
                               calender_style: "picker_4"
                           }, function (start, end, label) {
                               console.log(start.toISOString(), end.toISOString(), label);
                           });
                       });
                   </script>


                </div>
                <!-- /editor -->
          </div>
