<?php

	class M_Wisma extends CI_Model
	{

		public function getList()
		{
			$this->db->select('*');
			$this->db->from('wisma');


			$query = $this->db->get();
			if($query->num_rows()){
				return $query->result();
			}
			else return false;
		}

		public function getDetail($id)
		{
			$this->db->select('*');
			$this->db->from('wisma');
			$this->db->where('id_wisma', $id);
			$this->db->limit(1);


			$query = $this->db->get();
			if($query->num_rows()==1){
				return $query->result();
			}
			else return false;
		}

		public function update($id, $update)
		{

		}

		public function delete($id)
		{

		}


	}

 ?>
