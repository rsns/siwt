<!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Plain Page</h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" style="height:600px;">
															<div class="x_title">
                                    <h2>Bar Graph <small>Sessions</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <canvas id="canvas_bar"></canvas>
                                </div>
														</div>
                        </div>
                    </div>
                </div>

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

		<script>
			var randomScalingFactor = function () {
					return Math.round(Math.random() * 100)
			};



			var barChartData = {
					labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December"],
					datasets: [
							{
									fillColor: "#26B99A", //rgba(220,220,220,0.5)
									strokeColor: "#26B99A", //rgba(220,220,220,0.8)
									highlightFill: "#36CAAB", //rgba(220,220,220,0.75)
									highlightStroke: "#36CAAB", //rgba(220,220,220,1)
									data: [51, 30, 40, 28, 92, 50, 45,90,90,70,69,12]
					},
							{
									fillColor: "#03586A", //rgba(151,187,205,0.5)
									strokeColor: "#03586A", //rgba(151,187,205,0.8)
									highlightFill: "#066477", //rgba(151,187,205,0.75)
									highlightStroke: "#066477", //rgba(151,187,205,1)
									data: [41, 56, 25, 48, 72, 34, 12, 40, 28, 92, 50,30]
					}
			],
			}

      $(document).ready(function () {
          new Chart($("#canvas_bar").get(0).getContext("2d")).Bar(barChartData, {
              tooltipFillColor: "rgba(51, 51, 51, 0.55)",
              responsive: true,
              barDatasetSpacing: 6,
              barValueSpacing: 5
          });
      });
		</script>
