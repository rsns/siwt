<div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>
                    Tables
                    <small>
                        Some examples to get you started
                    </small>
                </h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>


                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Table design <small>Custom design</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="x_content">

                                    <p>Add class <code>bulk_action</code> to table for bulk actions options on row select</p>

                                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                        <thead>
                                            <tr class="headings">
                                                <th class="column-title">ID Transaksi </th>
                                                <th class="column-title">Tanggal Pembayaran </th>
                                                <th class="column-title">Atas Nama</th>
                                                <th class="column-title">Status </th>
                                                <th class="column-title">Amount </th>
                                            </th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr class="even pointer">
                                    <td class=" ">
										PBR000001
									</td>
                                    <td class=" ">
										23 Oktober 2015 11:47:56 PM
									</td>
                                    <td class=" ">
										John Blank L
									</td>
                                    <td class=" ">
										Lunas
									</td>
                                    <td class="a-right a-right ">
										Rp.  900.000
									</td>
                                </tr>
                                <tr class="odd pointer">
	                                <td class=" ">
										PBR000001
									</td>
	                                <td class=" ">
										24 November 2015 11:30:12 PM
									</td>
	                                <td class=" ">
										John Blank L
									</td>
	                                <td class=" ">
										Lunas
									</td>
	                                <td class="a-right a-right ">
	                                	Rp.  900.000
	                                </td>
	                            </tr>
								<tr class="even pointer">
									<td class=" ">
										PBR000001
									</td>
									<td class=" ">
										23 Oktober 2015 11:47:56 PM
									</td>
									<td class=" ">
										John Blank L
									</td>
									<td class=" ">
										Lunas
									</td>
									<td class="a-right a-right ">
										Rp.  900.000
									</td>
								</tr>
								<tr class="odd pointer">
									<td class=" ">
										PBR000001
									</td>
									<td class=" ">
										24 November 2015 11:30:12 PM
									</td>
									<td class=" ">
										John Blank L
									</td>
									<td class=" ">
										Lunas
									</td>
									<td class="a-right a-right ">
										Rp.  900.000
									</td>
								</tr>
	                                <tr class="even pointer">
	                                    <td class=" ">
											PBR000001
										</td>
	                                    <td class=" ">
											23 Oktober 2015 11:47:56 PM
										</td>
	                                    <td class=" ">
											John Blank L
										</td>
	                                    <td class=" ">
											Lunas
										</td>
	                                    <td class="a-right a-right ">
											Rp.  900.000
										</td>
	                                </tr>
	                                <tr class="odd pointer">
		                                <td class=" ">
											PBR000001
										</td>
		                                <td class=" ">
											24 November 2015 11:30:12 PM
										</td>
		                                <td class=" ">
											John Blank L
										</td>
		                                <td class=" ">
											Lunas
										</td>
		                                <td class="a-right a-right ">
		                                	Rp.  900.000
		                                </td>
		                            </tr>
									<tr class="even pointer">
										<td class=" ">
											PBR000001
										</td>
										<td class=" ">
											23 Oktober 2015 11:47:56 PM
										</td>
										<td class=" ">
											John Blank L
										</td>
										<td class=" ">
											Lunas
										</td>
										<td class="a-right a-right ">
											Rp.  900.000
										</td>
									</tr>
									<tr class="odd pointer">
										<td class=" ">
											PBR000001
										</td>
										<td class=" ">
											24 November 2015 11:30:12 PM
										</td>
										<td class=" ">
											John Blank L
										</td>
										<td class=" ">
											Lunas
										</td>
										<td class="a-right a-right ">
											Rp.  900.000
										</td>
									</tr>
		                                <tr class="even pointer">
		                                    <td class=" ">
												PBR000001
											</td>
		                                    <td class=" ">
												23 Oktober 2015 11:47:56 PM
											</td>
		                                    <td class=" ">
												John Blank L
											</td>
		                                    <td class=" ">
												Lunas
											</td>
		                                    <td class="a-right a-right ">
												Rp.  900.000
											</td>
		                                </tr>
		                                <tr class="odd pointer">
			                                <td class=" ">
												PBR000001
											</td>
			                                <td class=" ">
												24 November 2015 11:30:12 PM
											</td>
			                                <td class=" ">
												John Blank L
											</td>
			                                <td class=" ">
												Lunas
											</td>
			                                <td class="a-right a-right ">
			                                	Rp.  900.000
			                                </td>
			                            </tr>
										<tr class="even pointer">
											<td class=" ">
												PBR000001
											</td>
											<td class=" ">
												23 Oktober 2015 11:47:56 PM
											</td>
											<td class=" ">
												John Blank L
											</td>
											<td class=" ">
												Lunas
											</td>
											<td class="a-right a-right ">
												Rp.  900.000
											</td>
										</tr>
										<tr class="odd pointer">
											<td class=" ">
												PBR000001
											</td>
											<td class=" ">
												24 November 2015 11:30:12 PM
											</td>
											<td class=" ">
												John Blank L
											</td>
											<td class=" ">
												Lunas
											</td>
											<td class="a-right a-right ">
												Rp.  900.000
											</td>
										</tr>
							</tbody>

	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>

	</div>

	<!-- footer content -->
	<footer>
	    <div class="">
	        <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
	            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
	        </p>
	    </div>
	    <div class="clearfix"></div>
	</footer>
	<!-- /footer content -->

	</div>
