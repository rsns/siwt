<?php

defined('BASEPATH') OR exit('No direct script access allowed');

  class Barang extends CI_Model
  {
    var $table = 'barang';


    function __construct(){
      $this->db->from($this->table);
      $query = $this->db->get();
      $this->load->model('Barang');
      $this->load->helper('form','url');
      // echo print_r($data);
    }

    function getById($id)
    {
      $this->db->from($this->table);
      $this->db('where', $id);

      $query = $this->db->get();

      if($query)
        return $query->result();
      else
        return false;
    }

    function get()
    {
      $this->db->from($this->table);
      $query = $this->db->get();

      if($query)
        return $query->result();
      else
        return false;
    }

    function insert($id)
    {
      if($this->db->insert($this->table, $data))
        return true;
      else
        return false;

    }

    function delete($id){
      $test = $this->db->where('kode_brg',$id);

      if($test)
      {
        $test->delete($this->table);
        return True;
      }
      else {
        return False;
      }
    }
  }

 ?>
