<?php

	class M_Pemesanan extends CI_Model
	{

		public function getList()
		{
			$this->db->select('*');
			$this->db->from('pemesanan');
			$this->db->join('tamu', 'tamu.id_tamu = pemesanan.id_tamu');


			$query = $this->db->get();
			if($query->num_rows()){
				return $query->result();
			}
			else return false;
		}

		public function getDetail($id)
		{
			$this->db->select('*');
			$this->db->from('pemesanan');
			$this->db->where('id_pesan', $id);
			$this->db->limit(1);


			$query 		= $this->db->get();
			if($query->num_rows()==1){
				return $query->result();
			}
			else return false;
		}

		public function update($id, $update)
		{

		}

		public function delete($id)
		{

		}


	}

 ?>
