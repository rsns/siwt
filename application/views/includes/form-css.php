<!-- Custom styling plus plugins -->
      <link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">
      <link href="<?php echo base_url('assets/css/icheck/flat/green.css') ?>" rel="stylesheet">
      <!-- editor -->
      <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
      <link href="<?php echo base_url('assets/css/editor/external/google-code-prettify/prettify.css') ?>" rel="stylesheet">
      <link href="<?php echo base_url('assets/css/editor/index.css') ?>" rel="stylesheet">
      <!-- select2 -->
      <link href="<?php echo base_url('assets/css/select/select2.min.css') ?>" rel="stylesheet">
      <!-- switchery -->
      <link rel="stylesheet" href="<?php echo base_url('assets/css/switchery/switchery.min.css') ?>" />
