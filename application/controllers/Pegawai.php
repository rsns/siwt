<?php
//
// if(!isset($_SESSION))\

// require_once 'AdminController.php';
Class Pegawai extends CI_Controller{


	public function index(){
	  $this->load->library('session');
	  $data['auth'] = $this->session->logged_in;

      $this->load->view('includes/header')
                   ->view('includes/form-css')
                   ->view('partials/sidebar',$data)
                   ->view('partials/top-navigation',$data)
                   ->view('pegawai/dashboard',$data)
                   ->view('includes/scripts')
                   ->view('includes/footer')
                    ;
	}

	public function lihat(){
		$this->load->model('M_Pegawai');
		$this->load->library('session');
		$data['auth'] = $this->session->logged_in;
		$data['header'] = 'Pegawai';
		$data['title'] = 'Kelola Pegawai';
		$data['description'] = 'Tabel ini berisi daftar pegawai yang anda kelola.';

		$data['daftarpegawai'] = $this->M_Pegawai->getList();

      $this->load->view('includes/header')
                   ->view('includes/form-css')
                   ->view('partials/sidebar',$data)
                   ->view('partials/top-navigation',$data)
                   ->view('pegawai/index',$data)
                   ->view('includes/scripts')
                   ->view('includes/footer')
                    ;
	}

	public function tambah(){
		$this->load->library('session');
		$data['auth'] = $this->session->logged_in;
		$data['header'] = 'Pegawai';
		$data['title'] = 'Tambah Pegawai';
		$data['description'] = 'Masukkanlah data pegawai baru sesuai isian form berikut';

      	$this->load->view('includes/header')
                   ->view('includes/form-css')
                   ->view('partials/sidebar',$data)
                   ->view('partials/top-navigation',$data)
                   ->view('pegawai/tambah',$data)
                   ->view('includes/scripts')
                   ->view('includes/footer');

	}

	public function ubahPegawai()
	{

	}

	public function hapusPegawai()
	{

	}


}


 ?>
