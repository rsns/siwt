<!-- page content -->
											<div class="right_col" role="main">

															<div class="">
																			<div class="page-title">
																							<div class="title_left">
																											<h3><?php echo $header ?></h3>
																							</div>

																							<div class="title_right">
																											<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
																															<div class="input-group">
																																			<input type="text" class="form-control" placeholder="Search for...">
																																			<span class="input-group-btn">
																											<button class="btn btn-default" type="button">Go!</button>
																							</span>
																															</div>
																											</div>
																							</div>
																			</div>
																			<div class="clearfix"></div>

																			<div class="row">
																							<div class="col-md-12">
																											<div class="x_panel">
																															<div class="x_title">
																																			<h2><?php echo $title ?></h2>
																																			<ul class="nav navbar-right panel_toolbox">
																																							<li><a href="#"><i class="fa fa-chevron-up"></i></a>
																																							</li>
																																							<li class="dropdown">
																																											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
																																											<ul class="dropdown-menu" role="menu">
																																															<li><a href="#">Settings 1</a>
																																															</li>
																																															<li><a href="#">Settings 2</a>
																																															</li>
																																											</ul>
																																							</li>
																																							<li><a href="#"><i class="fa fa-close"></i></a>
																																							</li>
																																			</ul>
																																			<div class="clearfix"></div>
																															</div>

																															<div class="x_content">

																																			<div class="col-md-9 col-sm-9 col-xs-12">

																																				<ul class="stats-overview">
																																								<li>
																																												<span class="name"> ID Wisma </span>
																																												<span class="value text-success"> <?php echo $wisma->id_wisma ?> </span>
																																								</li>
																																								<li>
																																												<span class="name"> Status Sewa </span>
																																												<span class="value text-success"> <?php if($wisma->sewa_penuh=='0') echo 'Sewa Penuh'; else echo 'Sewa Per Kamar';?> </span>
																																								</li>
																																								<li class="hidden-phone">
																																												<span class="name"> Banyak Kamar </span>
																																												<span class="value text-success"> <?php  ?>
																																													<!-- TODO -->
																																												 </span>
																																								</li>
																																				</ul>
																																				<br />

																																				<div class="panel-body">
																																								<h3 class="green"><i class="fa fa-paint-brush"></i> <?php echo $wisma->nama_wisma ?></h3>

																																								<p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terr.</p>
																																								<br />

																																								<div class="project_detail">

																																												<p class="title">Alamat</p>
																																												<p><?php echo $wisma->alamat_wisma ?></p>
																																												<p class="title">Telefon Wisma</p>
																																												<p><?php echo $wisma->telp_wisma ?></p>
																																								</div>

																																				</div>

																																			</div>

																																			<!-- start project-detail sidebar -->
																																			<div class="col-md-3 col-sm-3 col-xs-12">

																																							<section class="panel">

																																											<div class="x_title">
																																															<h2>Informasi Kamar Wisma</h2>
																																															<!-- TODO -->
																																															<div class="clearfix"></div>
																																											</div>
																																											<div class="panel-body">
																																															<h3 class="green"><i class="fa fa-paint-brush"></i> Gentelella</h3>

																																															<p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terr.</p>
																																															<br />

																																															<div class="project_detail">

																																																			<p class="title">Client Company</p>
																																																			<p>Deveint Inc</p>
																																																			<p class="title">Project Leader</p>
																																																			<p>Tony Chicken</p>
																																															</div>

																																															<br />
																																															<h5>Project files</h5>
																																															<ul class="list-unstyled project_files">
																																																			<li><a href=""><i class="fa fa-file-word-o"></i> Functional-requirements.docx</a>
																																																			</li>
																																																			<li><a href=""><i class="fa fa-file-pdf-o"></i> UAT.pdf</a>
																																																			</li>
																																																			<li><a href=""><i class="fa fa-mail-forward"></i> Email-from-flatbal.mln</a>
																																																			</li>
																																																			<li><a href=""><i class="fa fa-picture-o"></i> Logo.png</a>
																																																			</li>
																																																			<li><a href=""><i class="fa fa-file-word-o"></i> Contract-10_12_2014.docx</a>
																																																			</li>
																																															</ul>
																																															<br />

																																															<div class="text-center mtop20">
																																																			<a href="#" class="btn btn-sm btn-primary">Add files</a>
																																																			<a href="#" class="btn btn-sm btn-warning">Report contact</a>
																																															</div>
																																											</div>

																																							</section>

																																			</div>
																																			<!-- end project-detail sidebar -->

																															</div>
																											</div>
																							</div>
																			</div>
															</div>

															<!-- footer content -->
															<footer>
																			<div class="">
																							<p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
																											<span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
																							</p>
																			</div>
																			<div class="clearfix"></div>
															</footer>
															<!-- /footer content -->

											</div>
											<!-- /page content -->
