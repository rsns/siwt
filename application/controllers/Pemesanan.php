<?php
//
// if(!isset($_SESSION))\

Class Pemesanan extends CI_Controller{

	//lihat pesanan
	public function index()
	{

			$this->load->model('M_Pemesanan');
		  $this->load->library('session');
		  $data['auth'] = $this->session->logged_in;
			$data['pesan'] = $this->M_Pemesanan->getList();
			$data['header'] = 'Pemesanan';
			$data['title'] = 'Kelola Pemesanan';
			$data['description'] = 'Halaman ini berisi pemesanan yang terdaftar dalam sistem';

			// return print_r($data['pesan']);
		  $this->load->library('session');
      $this->load->view('includes/header')
									->view('includes/calendar-style')
									->view('includes/scripts')
                   ->view('includes/form-css')
                   ->view('partials/sidebar',$data)
                   ->view('partials/top-navigation',$data)
                   ->view('pemesanan/index',$data)
                   ->view('includes/footer')
                    ;
	}

	public function detail($id)
	{

				$this->load->model('M_Pemesanan');
				$this->load->library('session');
				$data['auth'] = $this->session->logged_in;
				$data['pesan'] = $this->M_Pemesanan->getDetail($id)[0];
				$data['header'] = 'Pemesanan';
				$data['title'] = 'Detail Pemesanan';
				$data['description'] = 'Masukkanlah data wisma baru sesuai isian form berikut';

				if($data['pesan']){
					$this->load->view('includes/header')
											 ->view('includes/form-css')
											 ->view('partials/sidebar',$data)
											 ->view('partials/top-navigation',$data)
											 ->view('pemesanan/detail',$data)
											 ->view('includes/scripts')
											 ->view('includes/footer');
				}

	}

	public function update($id)
	{

	}

	public function hapusPemesanan()
	{

	}


}


 ?>
