<?php

	class M_Pegawai extends CI_Model
	{

		
		public function getList()
		{
			$this->db->select('id_petugas, nama_petugas, telp_petugas');
			$this->db->from('petugas_wisma');


			$query = $this->db->get();
			if($query->num_rows()){
				return $query->result();
			}
			else return false;
		}

		public function getDetail($id_petugas)
		{
			$this->db->select('*');
			$this->db->from('petugas_wisma');
			$this->db->where('id_petugas', $id_petugas);
			$this->db->limit(1);


			$query = $this->db->get();
			if($query->num_rows()==1){
				return $query->result();
			}
			else return false;
		}

		public function update($id, $update)
		{

		}

		public function delete($id)
		{

		}
	}

 ?>
