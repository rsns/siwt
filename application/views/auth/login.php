
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>DASHGUM - Bootstrap Admin Template</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url()?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url()?>assets/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/style-responsive.css" rel="stylesheet">

  </head>

  <body>

	  <div id="login-page">
	  	<div class="container">

		      <form class="form-login" method='POST' action="<?php echo base_url()?>authentication/try_login">

		        <h2 class="form-login-heading">sign in</h2>
		        <div class="login-wrap">
                 <?php
                     if(isset($error_message)){
                        echo "<div class='bg-danger'> ". $error_message . "</div><br>";

                     }
                     elseif(isset($message)){
                        echo "<div class='bg-success'> ". $message . "</div><br>";
                     }
                  ?>
                <i class="fa fa-user"></i>
                <label>
                  ID Pengenal
                </label>
		            <input type="text" class="form-control" placeholder='ID anda' name='identifier' autofocus required>
		            <br>
                <i class="fa fa-shield"></i>
                <label>
                  Password
                </label>
		            <input type="password" class="form-control" placeholder='password' name='password' required>
		            <br>
		            <button class="btn btn-theme btn-block" type="submit">
                  <i class="fa fa-lock"></i>
                    SIGN IN
                </button>

                  <br>

		            <div class="registration">
		                <br/>
		                <a data-toggle="modal" href="login.html#myModal">
		                    Laporkan Admin
		                </a>
		            </div>

		        </div>

		          <!-- Modal -->
		          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Laporkan Admin</h4>
		                      </div>
		                      <div class="modal-body ">
		                          <p>Masukkan NRP Anda : </p>
		                          <input type="text" name="email" placeholder="nrp_bermasalah" autocomplete="off" class="form-control placeholder-no-fix">

		                      </div>
		                      <div class="modal-footer">
		                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		                          <button class="btn btn-theme" type="button">Submit</button>
		                      </div>
		                  </div>
		              </div>
		          </div>
		          <!-- modal -->

		      </form>

	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("<?php echo base_url()?>assets/images/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>
