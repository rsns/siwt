<?php

	class M_Auth extends CI_Model
	{
		public function authenticate($identifier)
		{
			$this->db->select('*');
			$this->db->from('akun');
			$this->db->where('identifier',$identifier);
			$this->db->limit(1);

			$query = $this->db->get();
			if($query->num_rows()==1){
				return $query->result();
			}
			else return false;
		}

		public function getDetail($idpetugas)
		{
			$this->db->select('*');
			$this->db->from('petugas_wisma');
			$this->db->where('id_petugas',$idpetugas);
			$this->db->limit(1);


			$query = $this->db->get();
			if($query->num_rows()==1){
				return $query->result();
			}
			else return false;
		}

		public function registration($data)
		{

		}
	}

 ?>
