
    <script src="<?php echo base_url()?>/assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url()?>/assets/js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url()?>/assets/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url()?>/assets/js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url()?>/assets/js/icheck/icheck.min.js"></script>

    <script src="<?php echo base_url()?>/assets/js/custom.js"></script>
